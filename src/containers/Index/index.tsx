import React from 'react'
import styled from 'styled-components'
import PDF from './pdf'

export default function Index() {
  return (
    <Container>
      <PDF />
    </Container>
  )
}

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`
