import React, { useEffect } from 'react'
import styled from 'styled-components'
import ppk from 'pspdfkit'
import i90 from '../../../static/i90.pdf'

const licenseKey =
  'kP4_f_KZhhiHA3xldti3gHdqzosoFGG5bWDCw7pPrrN83RSEJMv7oiAm3DJeGoTIbOktxmgtxr_d9fnatNVtkPIjQE5LqEXPCSxKBoRlq-5P7M_sr4Mnog5W9BiO_Qf11nPBGzSDVZEDZ0BKRZ3QlavaOcIsCxTKznuala2djrXtKngSAOeQSJh-iyW8HaClXDGRA_byRoqNzpOVx6OLbPF4hhB6l_81Njg2C0guSlEGaf6vkFhFuQ_pYQZciu4K4FIVbW8ChsxHBfBmdAngbDrnwLF0QophYWE7EhmGdUH4hxLcq-BiQ_qmGyBvep7TbmbqD9BWdTLyrhHxDAZ9boISisc4nFcU-9hhliAwnb2I5ZAkOq0I5aba180eAsEXiQgslSUiPEq4rSDu6DC8pu0rfcs_a8upIJKbts8soNbym__AyetgtHViK6xoV0tC'

export default function PDF() {
  useEffect(() => {
    ppk.load({
      container: '#pdf-container',
      pdf: i90,
      licenseKey,
    })
  }, [])

  return <Container id="pdf-container" />
}

const Container = styled.div`
  width: 100%;
  height: 100vh;
`
