declare module '*.pdf' {
  let url: string
  export default url
}

declare module 'pspdfkit' {
  function load(options: {
    container: string
    pdf: string
    licenseKey: string
  }): Promise<any>
}
